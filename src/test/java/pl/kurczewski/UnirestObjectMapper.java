package pl.kurczewski;

import com.fasterxml.jackson.core.JsonProcessingException;
import kong.unirest.ObjectMapper;

import java.io.IOException;

public class UnirestObjectMapper implements ObjectMapper {

    private final com.fasterxml.jackson.databind.ObjectMapper jsonMapper;

    public UnirestObjectMapper(com.fasterxml.jackson.databind.ObjectMapper jsonMapper) {
        this.jsonMapper = jsonMapper;
    }

    @Override
    public <T> T readValue(String value, Class<T> valueType) {
        try {
            return jsonMapper.readValue(value, valueType);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String writeValue(Object value) {
        try {
            return jsonMapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}