package pl.kurczewski.concurrent;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import kong.unirest.Unirest;
import org.javamoney.moneta.Money;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.kurczewski.Main;
import pl.kurczewski.UnirestObjectMapper;
import pl.kurczewski.account.dto.Account;
import pl.kurczewski.account.dto.AccountForm;
import pl.kurczewski.json.MoneyAwareObjectMapper;
import pl.kurczewski.operation.dto.TransactForm;
import pl.kurczewski.operation.dto.TransferForm;
import pl.kurczewski.user.dto.User;
import pl.kurczewski.user.dto.UserForm;

import javax.money.Monetary;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.LongStream.rangeClosed;
import static org.assertj.core.api.Assertions.assertThat;

public class ConcurrencyTest {

    private static final int USERS = 30;
    private static final int TRANSFERS = 10000;
    private static final int TARGET_ACCOUNT_MISSES = 10;
    private static final Money FUND_PER_USER = Money.of(5, "USD");

    private final ObjectMapper objectMapper = MoneyAwareObjectMapper.create();
    private final Random random = new Random();

    private ExecutorService executor;

    @Before
    public void setUp() {
        Unirest.config().setObjectMapper(new UnirestObjectMapper(objectMapper));
        executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);
    }

    @After
    public void tearDown() {
        Unirest.shutDown();
        executor.shutdown();
    }

    @Test
    public void concurrencyTest() throws Exception {
        Main.main(new String[]{});

        assertThat(createUsers()
                .stream()
                .map(executor::submit)
                .map(this::unwrapFuture))
                .containsOnly(true);

        assertThat(createAccounts()
                .stream()
                .map(executor::submit)
                .map(this::unwrapFuture))
                .containsOnly(true);

        List<Callable<Boolean>> operations = new ArrayList<>();
        operations.addAll(depositFunds());
        operations.addAll(transferFunds());

        assertThat(operations.stream()
                .map(executor::submit)
                .map(this::unwrapFuture))
                .hasSize(TRANSFERS + USERS);

        verifyEndResult();
    }

    private List<Callable<Boolean>> createUsers() {
        return IntStream.rangeClosed(1, USERS)
                .mapToObj(index -> new UserForm("Test", "User", "test" + index + "@mail"))
                .map(userForm -> (Callable<Boolean>) () -> Unirest
                        .post("http://localhost:8080/users")
                        .body(userForm)
                        .asEmpty()
                        .isSuccess())
                .collect(Collectors.toList());
    }

    private List<Callable<Boolean>> createAccounts() {
        return IntStream.rangeClosed(1, USERS)
                .mapToObj(index -> new AccountForm(index, Monetary.getCurrency("USD")))
                .map(accountForm -> (Callable<Boolean>) () -> Unirest
                        .post("http://localhost:8080/accounts")
                        .body(accountForm)
                        .asEmpty()
                        .isSuccess())
                .collect(Collectors.toList());
    }

    private List<Callable<Boolean>> depositFunds() {
        return IntStream.rangeClosed(1, USERS)
                .mapToObj(index -> new TransactForm(index, FUND_PER_USER))
                .map(transactForm -> (Callable<Boolean>) () -> Unirest
                        .post("http://localhost:8080/operation/deposit")
                        .body(transactForm)
                        .asEmpty()
                        .isSuccess())
                .collect(Collectors.toList());
    }

    private List<Callable<Boolean>> transferFunds() {
        return IntStream.rangeClosed(1, TRANSFERS)
                .mapToObj(index -> {
                    int sourceAccountId = random.nextInt(USERS) + 1;
                    int targetAccountId = random.nextInt(USERS) + 1 + TARGET_ACCOUNT_MISSES;
                    int amount = random.nextInt(FUND_PER_USER.getNumberStripped().intValue()) + 1;
                    return new TransferForm(sourceAccountId, targetAccountId, Money.of(amount, "USD"));
                })
                .map(transferForm -> (Callable<Boolean>) () -> Unirest
                        .post("http://localhost:8080/operation/transfer")
                        .body(transferForm)
                        .asEmpty()
                        .isSuccess())
                .collect(Collectors.toList());
    }

    private void verifyEndResult() throws Exception {
        String usersJson = Unirest.get("http://localhost:8080/users").asString().getBody();
        List<User> users = objectMapper.readValue(usersJson, new TypeReference<List<User>>() {
        });
        String accountsJson = Unirest.get("http://localhost:8080/accounts").asString().getBody();
        List<Account> accounts = objectMapper.readValue(accountsJson, new TypeReference<List<Account>>() {
        });

        List<Long> expectedIds = rangeClosed(1, USERS).boxed().collect(Collectors.toList());

        assertThat(users.stream()
                .map(User::getId))
                .containsAll(expectedIds);
        assertThat(accounts.stream()
                .map(Account::getId))
                .containsAll(expectedIds);
        assertThat(accounts.stream()
                .map(Account::getBalance)
                .reduce(Money::add)
                .orElseThrow(NoSuchElementException::new))
                .isEqualTo(FUND_PER_USER.multiply(USERS));
    }

    private <T> T unwrapFuture(Future<T> future) {
        try {
            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
}
