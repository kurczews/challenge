# Instructions

## Running tests

```
mvn clean test
```

## Building jar

```
mvn clean package
```

## Running as standalone jar

Fat jar is available in `target/` folder:

```
java -jar ./target/revolut-challenge-1.0.jar
```

To be sure that classpath is not used sneakily:  

```
mv ./target/revolut-challenge-1.0.jar /tmp
cd /tmp
java -jar revolut-challenge-1.0.jar
```

NOTE: There are actually two JAR files in `target/` folder, I couldn't get rid of second one easily (would love to learn how fix this in clean manner)

## Final note

In this solution there are obviously issues related to atomicity of operations (think, sudden power outage in middle of transfer). I couldn't fix them easily without resorting to rewriting DB transaction logic or something like that so please keep that in mind.