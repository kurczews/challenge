package pl.kurczewski.user.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import pl.kurczewski.json.MoneyAwareObjectMapper;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class UserFormTest {

    private ObjectMapper jacksonJsonMapper = MoneyAwareObjectMapper.create();

    @Test
    public void serializeUserForm() throws JsonProcessingException {
        UserForm userForm = new UserForm("Bob", "Ross", "bob.ross@mail");
        assertThat(jacksonJsonMapper.writeValueAsString(userForm)).isEqualToIgnoringWhitespace("{\"name\":\"Bob\",\"surname\":\"Ross\",\"email\":\"bob.ross@mail\"}");
    }

    @Test
    public void deserializeUserForm() throws IOException {
        String payload = "{\"name\":\"Bob\",\"surname\":\"Ross\",\"email\":\"bob.ross@mail\"}";
        UserForm UserForm = jacksonJsonMapper.readValue(payload, UserForm.class);
        assertThat(UserForm.getName()).isEqualTo("Bob");
        assertThat(UserForm.getSurname()).isEqualTo("Ross");
        assertThat(UserForm.getEmail()).isEqualTo("bob.ross@mail");
    }
}