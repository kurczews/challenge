package pl.kurczewski.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.zalando.jackson.datatype.money.MoneyModule;

public class MoneyAwareObjectMapper {
    public static ObjectMapper create() {
        return new ObjectMapper().registerModule(new MoneyModule());
    }
}