package pl.kurczewski;

import io.javalin.Javalin;
import io.javalin.plugin.json.JavalinJackson;
import pl.kurczewski.account.AccountController;
import pl.kurczewski.account.AccountStorage;
import pl.kurczewski.json.MoneyAwareObjectMapper;
import pl.kurczewski.operation.OperationController;
import pl.kurczewski.user.UserController;
import pl.kurczewski.user.UserStorage;

public class Main {
    public static void main(String[] args) {
        UserStorage userStorage = new UserStorage();
        UserController userController = new UserController(userStorage);
        AccountStorage accountStorage = new AccountStorage(userStorage);
        AccountController accountController = new AccountController(accountStorage);
        OperationController operationController = new OperationController(accountStorage);

        Javalin server = Javalin.create().start(8080);
        JavalinJackson.configure(MoneyAwareObjectMapper.create());

        userController.register(server);
        accountController.register(server);
        operationController.register(server);
    }
}
