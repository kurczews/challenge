package pl.kurczewski.user;

import pl.kurczewski.user.dto.User;
import pl.kurczewski.user.dto.UserForm;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class UserStorage {

    private ConcurrentHashMap<Long, User> users = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, Boolean> emails = new ConcurrentHashMap<>();
    private AtomicLong idSequence = new AtomicLong(0);

    public boolean contains(long id) {
        return users.containsKey(id);
    }

    /**
     * @param userForm data needed for user creation
     * @return id created for user or empty optional in case user email is occupied
     */
    public Optional<Long> register(UserForm userForm) {
        if (emails.putIfAbsent(userForm.getEmail(), true) != null) {
            return Optional.empty();
        }
        long id = idSequence.incrementAndGet();
        users.put(id, new User(id, userForm));
        return Optional.of(id);
    }

    Collection<User> getUsers() {
        return Collections.unmodifiableCollection(users.values());
    }

    Optional<User> find(long id) {
        return Optional.ofNullable(users.get(id));
    }
}