package pl.kurczewski.account.dto;

import org.javamoney.moneta.Money;

public class Account {

    private long id;
    private long userId;
    private Money balance;

    private Account() {
        // for jackson
    }

    private Account(long id, long userId, Money balance) {
        this.id = id;
        this.userId = userId;
        this.balance = balance;
    }

    public Account(long id, AccountForm accountForm) {
        this(id, accountForm.getUserId(), Money.zero(accountForm.getCurrency()));
    }

    public Account updateBalance(Money balance) {
        return new Account(id, userId, balance);
    }

    public long getId() {
        return id;
    }

    public long getUserId() {
        return userId;
    }

    public Money getBalance() {
        return balance;
    }
}
