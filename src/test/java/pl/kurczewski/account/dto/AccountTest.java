package pl.kurczewski.account.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import pl.kurczewski.json.MoneyAwareObjectMapper;

import javax.money.Monetary;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountTest {

    private ObjectMapper jacksonJsonMapper = MoneyAwareObjectMapper.create();

    @Test
    public void serializeAccount() throws JsonProcessingException {
        Account account = new Account(1, new AccountForm(1, Monetary.getCurrency("USD")));
        assertThat(jacksonJsonMapper.writeValueAsString(account))
                .isEqualToIgnoringWhitespace("{\"id\":1,\"userId\":1,\"balance\":{\"amount\":0.00,\"currency\":\"USD\"}}");
    }
}