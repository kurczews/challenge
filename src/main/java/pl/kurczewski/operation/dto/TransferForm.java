package pl.kurczewski.operation.dto;

import org.javamoney.moneta.Money;

public class TransferForm {

    private long sourceAccountId;
    private long targetAccountId;
    private Money amount;

    private TransferForm() {
        // for jackson
    }

    public TransferForm(long sourceAccountId, long targetAccountId, Money amount) {
        this.sourceAccountId = sourceAccountId;
        this.targetAccountId = targetAccountId;
        this.amount = amount;
    }

    public long getSourceAccountId() {
        return sourceAccountId;
    }

    public long getTargetAccountId() {
        return targetAccountId;
    }

    public Money getAmount() {
        return amount;
    }
}
