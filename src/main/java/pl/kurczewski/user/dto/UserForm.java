package pl.kurczewski.user.dto;

public class UserForm {

    private String name;
    private String surname;
    private String email;

    private UserForm() {
        // for jackson
    }

    public UserForm(String name, String surname, String email) {
        this.name = name;
        this.surname = surname;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }
}
