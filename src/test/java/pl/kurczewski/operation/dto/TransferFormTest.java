package pl.kurczewski.operation.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.javamoney.moneta.Money;
import org.junit.Test;
import pl.kurczewski.json.MoneyAwareObjectMapper;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class TransferFormTest {

    private ObjectMapper jacksonJsonMapper = MoneyAwareObjectMapper.create();

    @Test
    public void serializeTransferForm() throws JsonProcessingException {
        TransferForm transferForm = new TransferForm(1, 2, Money.of(5, "USD"));
        assertThat(jacksonJsonMapper.writeValueAsString(transferForm)).isEqualToIgnoringWhitespace("{\"sourceAccountId\":1,\"targetAccountId\":2,\"amount\":{\"amount\":5.00,\"currency\":\"USD\"}}");
    }

    @Test
    public void deserializeTransferForm() throws IOException {
        String payload = "{\"sourceAccountId\":1,\"targetAccountId\":2,\"amount\":{\"amount\":5.00,\"currency\":\"USD\"}}";
        TransferForm transferForm = jacksonJsonMapper.readValue(payload, TransferForm.class);
        assertThat(transferForm.getSourceAccountId()).isEqualTo(1);
        assertThat(transferForm.getTargetAccountId()).isEqualTo(2);
        assertThat(transferForm.getAmount()).isEqualTo(Money.of(5, "USD"));
    }
}