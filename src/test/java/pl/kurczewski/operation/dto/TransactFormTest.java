package pl.kurczewski.operation.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.javamoney.moneta.Money;
import org.junit.Test;
import pl.kurczewski.json.MoneyAwareObjectMapper;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class TransactFormTest {
    
    private ObjectMapper jacksonJsonMapper = MoneyAwareObjectMapper.create();

    @Test
    public void serializeTransactForm() throws JsonProcessingException {
        TransactForm transactForm = new TransactForm(1, Money.of(5, "USD"));
        assertThat(jacksonJsonMapper.writeValueAsString(transactForm)).isEqualToIgnoringWhitespace("{\"accountId\":1,\"amount\":{\"amount\":5.00,\"currency\":\"USD\"}}");
    }

    @Test
    public void deserializeTransactForm() throws IOException {
        String payload = "{\"accountId\":1,\"amount\":{\"amount\":5.00,\"currency\":\"USD\"}}";
        TransactForm transactForm = jacksonJsonMapper.readValue(payload, TransactForm.class);
        assertThat(transactForm.getAccountId()).isEqualTo(1);
        assertThat(transactForm.getAmount()).isEqualTo(Money.of(5, "USD"));
    }
}