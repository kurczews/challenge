package pl.kurczewski.user;

import io.javalin.Javalin;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.kurczewski.json.MoneyAwareObjectMapper;
import pl.kurczewski.user.dto.User;
import pl.kurczewski.user.dto.UserForm;
import pl.kurczewski.UnirestObjectMapper;

import java.util.ArrayList;
import java.util.List;

import static java.net.HttpURLConnection.*;
import static org.assertj.core.api.Assertions.assertThat;

public class UserControllerTest {

    private Javalin server;

    @Before
    public void setUp() {
        server = Javalin.create().start(1234);
        Unirest.config().setObjectMapper(new UnirestObjectMapper(MoneyAwareObjectMapper.create()));
    }

    @After
    public void tearDown() {
        server.stop();
        Unirest.shutDown();
    }

    @Test
    public void shouldReturnNoUsers() {
        new UserController(new UserStorage()).register(server);

        HttpResponse<String> response = Unirest.get("http://localhost:1234/users").asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_OK);
        assertThat(response.getBody()).isEqualTo("[]");
    }

    @Test
    public void shouldAddUser() {
        UserStorage userStorage = new UserStorage();
        UserForm userForm = new UserForm("Bob", "Ross", "bob.ross@mail");

        new UserController(userStorage).register(server);

        HttpResponse<String> response = Unirest.post("http://localhost:1234/users").body(userForm).asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_CREATED);
        assertThat(response.getBody()).isEqualTo("/users/1");

        List<User> users = new ArrayList<>(userStorage.getUsers());
        assertThat(users).hasSize(1);

        User storedUser = users.get(0);
        assertThat(storedUser.getId()).isEqualTo(1);
        assertThat(storedUser.getName()).isEqualTo("Bob");
        assertThat(storedUser.getSurname()).isEqualTo("Ross");
        assertThat(storedUser.getEmail()).isEqualTo("bob.ross@mail");
    }

    @Test
    public void shouldReturnListOfUsers() {
        UserStorage userStorage = new UserStorage();
        UserForm userForm = new UserForm("Bob", "Ross", "bob.ross@mail");
        userStorage.register(userForm);

        new UserController(userStorage).register(server);

        HttpResponse<String> response = Unirest.get("http://localhost:1234/users").asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_OK);
        assertThat(response.getBody()).isEqualTo("[{\"id\":1,\"name\":\"Bob\",\"surname\":\"Ross\",\"email\":\"bob.ross@mail\"}]");
    }

    @Test
    public void shouldReturnGivenUser() {
        UserStorage userStorage = new UserStorage();
        UserForm userForm = new UserForm("Bob", "Ross", "bob.ross@mail");
        userStorage.register(userForm);

        new UserController(userStorage).register(server);

        HttpResponse<String> response = Unirest.get("http://localhost:1234/users/1").asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_OK);
        assertThat(response.getBody()).isEqualTo("{\"id\":1,\"name\":\"Bob\",\"surname\":\"Ross\",\"email\":\"bob.ross@mail\"}");
    }

    @Test
    public void shouldReturnNotFoundStatusWhenGivenUserDoesNotExist() {
        UserStorage userStorage = new UserStorage();
        new UserController(userStorage).register(server);

        HttpResponse<String> response = Unirest.get("http://localhost:1234/users/1").asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_NOT_FOUND);
        assertThat(response.getBody()).isEqualTo("/users/1");
    }

    @Test
    public void shouldNotRegisterDuplicateMail() {
        UserStorage userStorage = new UserStorage();
        UserForm userForm = new UserForm("Bob", "Ross", "bob.ross@mail");
        userStorage.register(userForm);

        UserController userController = new UserController(userStorage);
        userController.register(server);

        HttpResponse<String> response = Unirest.post("http://localhost:1234/users").body(userForm).asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_CONFLICT);
        assertThat(response.getBody()).isEmpty();
        assertThat(userStorage.getUsers()).hasSize(1);
    }
}