package pl.kurczewski.account;

import io.javalin.Javalin;
import io.javalin.plugin.json.JavalinJackson;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.kurczewski.account.dto.Account;
import pl.kurczewski.account.dto.AccountForm;
import pl.kurczewski.json.MoneyAwareObjectMapper;
import pl.kurczewski.user.UserStorage;
import pl.kurczewski.user.dto.UserForm;
import pl.kurczewski.UnirestObjectMapper;

import javax.money.Monetary;
import java.util.ArrayList;
import java.util.List;

import static java.net.HttpURLConnection.*;
import static org.assertj.core.api.Assertions.assertThat;

public class AccountControllerTest {

    private Javalin server;

    @Before
    public void setUp() {
        server = Javalin.create().start(1234);
        JavalinJackson.configure(MoneyAwareObjectMapper.create());
        Unirest.config().setObjectMapper(new UnirestObjectMapper(MoneyAwareObjectMapper.create()));
    }

    @After
    public void tearDown() {
        server.stop();
        Unirest.shutDown();
    }

    @Test
    public void shouldReturnNoAccounts() {
        AccountStorage accountStorage = emptyAccountStorageWithUser();
        new AccountController(accountStorage).register(server);

        HttpResponse<String> response = Unirest.get("http://localhost:1234/accounts").asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_OK);
        assertThat(response.getBody()).isEqualTo("[]");
    }

    @Test
    public void shouldAddAccountForExistingUser() {
        AccountStorage accountStorage = emptyAccountStorageWithUser();
        AccountForm accountForm = new AccountForm(1, Monetary.getCurrency("USD"));

        new AccountController(accountStorage).register(server);

        HttpResponse<String> response = Unirest.post("http://localhost:1234/accounts").body(accountForm).asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_CREATED);
        assertThat(response.getBody()).isEqualTo("/accounts/1");

        List<Account> accounts = new ArrayList<>(accountStorage.getAccounts());
        assertThat(accounts).hasSize(1);

        Account storedAccount = accounts.get(0);
        assertThat(storedAccount.getId()).isEqualTo(1);
        assertThat(storedAccount.getUserId()).isEqualTo(1);
        assertThat(storedAccount.getBalance().toString()).isEqualTo("USD 0");
    }

    @Test
    public void shouldReturnListOfAccounts() {
        AccountStorage accountStorage = singleAccountStorage();
        new AccountController(accountStorage).register(server);

        HttpResponse<String> response = Unirest.get("http://localhost:1234/accounts").asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_OK);
        assertThat(response.getBody()).isEqualTo("[{\"id\":1,\"userId\":1,\"balance\":{\"amount\":0.00,\"currency\":\"USD\"}}]");
    }

    @Test
    public void shouldReturnGivenAccount() {
        AccountStorage accountStorage = singleAccountStorage();
        new AccountController(accountStorage).register(server);

        HttpResponse<String> response = Unirest.get("http://localhost:1234/accounts/1").asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_OK);
        assertThat(response.getBody()).isEqualTo("{\"id\":1,\"userId\":1,\"balance\":{\"amount\":0.00,\"currency\":\"USD\"}}");
    }

    @Test
    public void shouldReturnNotFoundStatusWhenGivenAccountDoesNotExists() {
        AccountStorage accountStorage = emptyAccountStorageWithoutUser();
        new AccountController(accountStorage).register(server);

        HttpResponse<String> response = Unirest.get("http://localhost:1234/accounts/1").asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_NOT_FOUND);
        assertThat(response.getBody()).isEqualTo("/accounts/1");
    }

    @Test
    public void shouldReturnUserAccounts() {
        AccountStorage accountStorage = singleAccountStorage();
        new AccountController(accountStorage).register(server);

        HttpResponse<String> response = Unirest.get("http://localhost:1234/accounts/users/1").asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_OK);
        assertThat(response.getBody()).isEqualTo("[{\"id\":1,\"userId\":1,\"balance\":{\"amount\":0.00,\"currency\":\"USD\"}}]");
    }

    @Test
    public void shouldReturnNotFoundStatusWhenGivenUserDoesNotExist() {
        AccountStorage accountStorage = emptyAccountStorageWithoutUser();
        new AccountController(accountStorage).register(server);

        HttpResponse<String> response = Unirest.get("http://localhost:1234/accounts/users/1").asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_NOT_FOUND);
        assertThat(response.getBody()).isEqualTo("/users/1");
    }

    @Test
    public void shouldNotCreateAccountIfUserDoesNotExists() {
        AccountStorage accountStorage = emptyAccountStorageWithoutUser();
        new AccountController(accountStorage).register(server);

        AccountForm accountForm = new AccountForm(1, Monetary.getCurrency("USD"));

        HttpResponse<String> response = Unirest.post("http://localhost:1234/accounts").body(accountForm).asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_NOT_FOUND);
        assertThat(response.getBody()).isEqualTo("/users/1");

        assertThat(accountStorage.getAccounts()).isEmpty();
    }

    private AccountStorage singleAccountStorage() {
        UserStorage userStorage = new UserStorage();
        userStorage.register(new UserForm("Bob", "Ross", "bob.ross@mail"));

        AccountStorage accountStorage = new AccountStorage(userStorage);
        AccountForm accountForm = new AccountForm(1, Monetary.getCurrency("USD"));
        accountStorage.createNewAccount(accountForm);

        return accountStorage;
    }

    private AccountStorage emptyAccountStorageWithUser() {
        UserStorage userStorage = new UserStorage();
        userStorage.register(new UserForm("Bob", "Ross", "bob.ross@mail"));

        return new AccountStorage(userStorage);
    }

    private AccountStorage emptyAccountStorageWithoutUser() {
        return new AccountStorage(new UserStorage());
    }
}