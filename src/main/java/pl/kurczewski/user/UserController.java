package pl.kurczewski.user;

import io.javalin.Javalin;
import pl.kurczewski.adapter.JavalinController;
import pl.kurczewski.user.dto.User;
import pl.kurczewski.user.dto.UserForm;

import java.util.Optional;

import static java.net.HttpURLConnection.*;

public class UserController implements JavalinController {

    public static final String USER_URI = "/users";
    private final UserStorage userStorage;

    public UserController(UserStorage userStorage) {
        this.userStorage = userStorage;
    }

    @Override
    public void register(Javalin routes) {
        this.getUsers(routes);
        this.getUser(routes);
        this.createUser(routes);
    }

    private void getUsers(Javalin routes) {
        routes.get(USER_URI, (ctx) -> ctx.status(HTTP_OK).json(userStorage.getUsers()));
    }

    private void getUser(Javalin routes) {
        routes.get(USER_URI + "/:id", (ctx) -> {
            long id = ctx.pathParam("id", Long.class).get();
            Optional<User> user = userStorage.find(id);
            if (user.isPresent()) {
                ctx.status(HTTP_OK).json(user.get());
            } else {
                ctx.status(HTTP_NOT_FOUND).result(USER_URI + "/" + id);
            }
        });
    }

    private void createUser(Javalin routes) {
        routes.post(USER_URI, (ctx) -> {
            UserForm userForm = ctx.bodyAsClass(UserForm.class);
            Optional<Long> id = userStorage.register(userForm);
            if (id.isPresent()) {
                ctx.status(HTTP_CREATED).result(USER_URI + "/" + id.get());
            } else {
                ctx.status(HTTP_CONFLICT);
            }
        });
    }
}