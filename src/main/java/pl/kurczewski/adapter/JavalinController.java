package pl.kurczewski.adapter;

import io.javalin.Javalin;

public interface JavalinController {
    void register(Javalin routes);
}
