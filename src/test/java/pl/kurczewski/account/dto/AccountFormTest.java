package pl.kurczewski.account.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import pl.kurczewski.json.MoneyAwareObjectMapper;

import javax.money.Monetary;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountFormTest {

    private ObjectMapper jacksonJsonMapper = MoneyAwareObjectMapper.create();

    @Test
    public void serializeAccountForm() throws JsonProcessingException {
        AccountForm accountForm = new AccountForm(1, Monetary.getCurrency("USD"));
        assertThat(jacksonJsonMapper.writeValueAsString(accountForm)).isEqualToIgnoringWhitespace("{\"userId\":1, \"currency\":\"USD\"}");
    }

    @Test
    public void deserializeAccountForm() throws IOException {
        String payload = "{\"userId\":1, \"currency\":\"USD\"}";
        AccountForm accountForm = jacksonJsonMapper.readValue(payload, AccountForm.class);
        assertThat(accountForm.getUserId()).isEqualTo(1);
        assertThat(accountForm.getCurrency().getCurrencyCode()).isEqualTo("USD");
    }
}