package pl.kurczewski.operation.dto;

import org.javamoney.moneta.Money;

public class TransactForm {

    private long accountId;
    private Money amount;

    private TransactForm() {
        // for jackson
    }

    public TransactForm(long accountId, Money amount) {
        this.accountId = accountId;
        this.amount = amount;
    }

    public long getAccountId() {
        return accountId;
    }

    public Money getAmount() {
        return amount;
    }
}
