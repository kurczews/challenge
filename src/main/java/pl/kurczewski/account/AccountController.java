package pl.kurczewski.account;

import io.javalin.Javalin;
import io.javalin.http.Context;
import pl.kurczewski.account.dto.Account;
import pl.kurczewski.account.dto.AccountForm;
import pl.kurczewski.adapter.JavalinController;
import pl.kurczewski.user.UserController;

import java.util.List;
import java.util.Optional;

import static java.net.HttpURLConnection.*;

public class AccountController implements JavalinController {

    public static final String ACCOUNTS_URI = "/accounts";
    private final AccountStorage accountStorage;

    public AccountController(AccountStorage accountStorage) {
        this.accountStorage = accountStorage;
    }

    @Override
    public void register(Javalin routes) {
        this.getAllAccounts(routes);
        this.getAccount(routes);
        this.getUserAccounts(routes);
        this.createAccount(routes);
    }

    private void getAllAccounts(Javalin routes) {
        routes.get(ACCOUNTS_URI, (ctx) -> ctx.status(HTTP_OK).json(accountStorage.getAccounts()));
    }

    private void getAccount(Javalin routes) {
        routes.get(ACCOUNTS_URI + "/:accountId", (ctx) -> {
            long accountId = ctx.pathParam("accountId", Long.class).get();
            Optional<Account> account = accountStorage.findById(accountId);
            if (account.isPresent()) {
                ctx.status(HTTP_OK).json(account.get());
            } else {
                ctx.status(HTTP_NOT_FOUND).result(ACCOUNTS_URI + "/" + accountId);
            }
        });
    }

    private void getUserAccounts(Javalin routes) {
        routes.get(ACCOUNTS_URI + UserController.USER_URI + "/:userId", (ctx) -> {
            long userId = ctx.pathParam("userId", Long.class).get();
            Optional<List<Account>> userAccounts = accountStorage.findByOwner(userId);
            if (userAccounts.isPresent()) {
                ctx.status(HTTP_OK).json(userAccounts.get());
            } else {
                userNotExists(ctx, userId);
            }
        });
    }

    private void createAccount(Javalin routes) {
        routes.post(ACCOUNTS_URI, (ctx) -> {
            AccountForm accountForm = ctx.bodyAsClass(AccountForm.class);
            Optional<Long> id = accountStorage.createNewAccount(accountForm);
            if (id.isPresent()) {
                ctx.status(HTTP_CREATED).result(ACCOUNTS_URI + "/" + id.get());
            } else {
                userNotExists(ctx, accountForm.getUserId());
            }
        });
    }

    private void userNotExists(Context ctx, long userId) {
        ctx.status(HTTP_NOT_FOUND).result(UserController.USER_URI + "/" + userId);
    }
}
