package pl.kurczewski.account.dto;

import javax.money.CurrencyUnit;

public class AccountForm {

    private long userId;
    private CurrencyUnit currency;

    private AccountForm() {
        // for jackson
    }

    public AccountForm(long userId, CurrencyUnit currency) {
        this.userId = userId;
        this.currency = currency;
    }

    public long getUserId() {
        return userId;
    }

    public CurrencyUnit getCurrency() {
        return currency;
    }
}
