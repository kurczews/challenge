package pl.kurczewski.operation;

import io.javalin.Javalin;
import pl.kurczewski.account.AccountStorage;
import pl.kurczewski.account.exception.AccountNotExistsException;
import pl.kurczewski.account.exception.InsufficientBalanceException;
import pl.kurczewski.adapter.JavalinController;
import pl.kurczewski.operation.dto.TransactForm;
import pl.kurczewski.operation.dto.TransferForm;

import static java.net.HttpURLConnection.*;
import static pl.kurczewski.account.AccountController.ACCOUNTS_URI;

public class OperationController implements JavalinController {

    private static final String OPERATION_URI = "/operation";
    private static final String WITHDRAW_URI = "/withdraw";
    private static final String DEPOSIT_URI = "/deposit";
    private static final String TRANSFER_URI = "/transfer";

    private final AccountStorage accountStorage;

    public OperationController(AccountStorage accountStorage) {
        this.accountStorage = accountStorage;
    }

    @Override
    public void register(Javalin routes) {
        this.deposit(routes);
        this.withdraw(routes);
        this.transfer(routes);
    }

    private void deposit(Javalin routes) {
        routes.post(OPERATION_URI + DEPOSIT_URI, (ctx) -> {
            TransactForm transactForm = ctx.bodyAsClass(TransactForm.class);
            boolean succeed = accountStorage.deposit(transactForm.getAccountId(), transactForm.getAmount());
            if (succeed) {
                ctx.status(HTTP_NO_CONTENT);
            } else {
                ctx.status(HTTP_NOT_FOUND).result(ACCOUNTS_URI + "/" + transactForm.getAccountId());
            }
        });
    }

    private void withdraw(Javalin routes) {
        routes.post(OPERATION_URI + WITHDRAW_URI, (ctx) -> {
            TransactForm transactForm = ctx.bodyAsClass(TransactForm.class);
            boolean succeed;
            try {
                succeed = accountStorage.withdraw(transactForm.getAccountId(), transactForm.getAmount());
            } catch (InsufficientBalanceException ex) {
                ctx.status(HTTP_BAD_REQUEST).result("Insufficient balance to perform withdraw action");
                return;
            }
            if (succeed) {
                ctx.status(HTTP_NO_CONTENT);
            } else {
                ctx.status(HTTP_NOT_FOUND).result(ACCOUNTS_URI + "/" + transactForm.getAccountId());
            }
        });
    }

    private void transfer(Javalin routes) {
        routes.post(OPERATION_URI + TRANSFER_URI, (ctx) -> {
            TransferForm transferForm = ctx.bodyAsClass(TransferForm.class);
            boolean succeed;
            try {
                succeed = accountStorage.transfer(
                        transferForm.getSourceAccountId(),
                        transferForm.getTargetAccountId(),
                        transferForm.getAmount()
                );
            } catch (AccountNotExistsException ex) {
                ctx.status(HTTP_NOT_FOUND).result(ACCOUNTS_URI + "/" + transferForm.getTargetAccountId());
                return;
            } catch (InsufficientBalanceException ex) {
                ctx.status(HTTP_BAD_REQUEST).result("Insufficient balance to perform transfer action");
                return;
            }
            if (succeed) {
                ctx.status(HTTP_NO_CONTENT);
            } else {
                ctx.status(HTTP_NOT_FOUND).result(ACCOUNTS_URI + "/" + transferForm.getSourceAccountId());
            }
        });
    }
}
