package pl.kurczewski.operation;

import io.javalin.Javalin;
import io.javalin.plugin.json.JavalinJackson;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.javamoney.moneta.Money;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.kurczewski.UnirestObjectMapper;
import pl.kurczewski.account.AccountStorage;
import pl.kurczewski.account.dto.AccountForm;
import pl.kurczewski.json.MoneyAwareObjectMapper;
import pl.kurczewski.operation.dto.TransactForm;
import pl.kurczewski.operation.dto.TransferForm;
import pl.kurczewski.user.UserStorage;
import pl.kurczewski.user.dto.UserForm;

import javax.money.Monetary;
import java.util.NoSuchElementException;

import static java.net.HttpURLConnection.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

public class OperationControllerTest {

    private Javalin server;

    @Before
    public void setUp() {
        server = Javalin.create().start(1234);
        JavalinJackson.configure(MoneyAwareObjectMapper.create());
        Unirest.config().setObjectMapper(new UnirestObjectMapper(MoneyAwareObjectMapper.create()));
    }

    @After
    public void tearDown() {
        server.stop();
        Unirest.shutDown();
    }

    @Test
    public void shouldDepositGivenMoneyOnAccount() {
        AccountStorage accountStorage = singleAccountStorage();
        new OperationController(accountStorage).register(server);

        TransactForm transactForm = new TransactForm(1, Money.of(5, "USD"));
        HttpResponse<String> response = Unirest
                .post("http://localhost:1234/operation/deposit")
                .body(transactForm)
                .asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_NO_CONTENT);

        assertThat(accountStorage
                .findById(1)
                .orElseThrow(NoSuchElementException::new)
                .getBalance())
                .isEqualTo(Money.of(5, "USD"));
    }

    @Test
    public void shouldWithdrawGivenMoneyOnAccount() {
        AccountStorage accountStorage = singleAccountStorage();
        new OperationController(accountStorage).register(server);
        Money amount = Money.of(5, "USD");
        accountStorage.deposit(1, amount);

        TransactForm transactForm = new TransactForm(1, amount);
        HttpResponse<String> response = Unirest
                .post("http://localhost:1234/operation/withdraw")
                .body(transactForm)
                .asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_NO_CONTENT);

        assertThat(accountStorage
                .findById(1)
                .orElseThrow(NoSuchElementException::new)
                .getBalance())
                .isEqualTo(Money.of(0, "USD"));
    }

    @Test
    public void shouldTransferGivenMoneyBetweenAccounts() {
        AccountStorage accountStorage = singleAccountStorage();
        AccountForm targetAccount = new AccountForm(1, Monetary.getCurrency("USD"));
        accountStorage.createNewAccount(targetAccount);
        Money amount = Money.of(5, "USD");
        accountStorage.deposit(1, amount);

        new OperationController(accountStorage).register(server);

        TransferForm transferForm = new TransferForm(1, 2, amount);
        HttpResponse<String> response = Unirest
                .post("http://localhost:1234/operation/transfer")
                .body(transferForm)
                .asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_NO_CONTENT);

        assertThat(accountStorage
                .findById(1)
                .orElseThrow(NoSuchElementException::new)
                .getBalance())
                .isEqualTo(Money.of(0, "USD"));
        assertThat(accountStorage
                .findById(2)
                .orElseThrow(NoSuchElementException::new)
                .getBalance())
                .isEqualTo(Money.of(5, "USD"));
    }

    @Test
    public void shouldNotDepositOnNonExistingAccount() {
        AccountStorage accountStorage = emptyAccountStorage();
        new OperationController(accountStorage).register(server);

        TransactForm transactForm = new TransactForm(1, Money.of(5, "USD"));
        HttpResponse<String> response = Unirest
                .post("http://localhost:1234/operation/deposit")
                .body(transactForm)
                .asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_NOT_FOUND);
        assertThat(response.getBody()).isEqualTo("/accounts/1");
    }

    @Test
    public void shouldNotWithdrawWhenInsufficientBalance() {
        AccountStorage accountStorage = singleAccountStorage();
        new OperationController(accountStorage).register(server);

        TransactForm transactForm = new TransactForm(1, Money.of(5, "USD"));
        HttpResponse<String> response = Unirest
                .post("http://localhost:1234/operation/withdraw")
                .body(transactForm)
                .asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_BAD_REQUEST);
        assertThat(response.getBody()).isEqualTo("Insufficient balance to perform withdraw action");
    }

    @Test
    public void shouldNotWithdrawFromNonExistingAccount() {
        AccountStorage accountStorage = emptyAccountStorage();
        new OperationController(accountStorage).register(server);

        TransactForm transactForm = new TransactForm(1, Money.of(5, "USD"));
        HttpResponse<String> response = Unirest
                .post("http://localhost:1234/operation/withdraw")
                .body(transactForm)
                .asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_NOT_FOUND);
        assertThat(response.getBody()).isEqualTo("/accounts/1");
    }

    @Test
    public void shouldNotTransferFromNonExistingAccount() {
        AccountStorage accountStorage = emptyAccountStorage();
        new OperationController(accountStorage).register(server);

        TransferForm transferForm = new TransferForm(1, 2, Money.of(5, "USD"));
        HttpResponse<String> response = Unirest
                .post("http://localhost:1234/operation/transfer")
                .body(transferForm)
                .asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_NOT_FOUND);
        assertThat(response.getBody()).isEqualTo("/accounts/1");
    }

    @Test
    public void shouldNotTransferWhenInsufficientBalance() {
        AccountStorage accountStorage = singleAccountStorage();
        new OperationController(accountStorage).register(server);

        TransferForm transferForm = new TransferForm(1, 2, Money.of(5, "USD"));
        HttpResponse<String> response = Unirest
                .post("http://localhost:1234/operation/transfer")
                .body(transferForm)
                .asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_BAD_REQUEST);
        assertThat(response.getBody()).isEqualTo("Insufficient balance to perform transfer action");
    }

    @Test
    public void shouldNotTransferToNonExistingAccount() {
        AccountStorage accountStorage = singleAccountStorage();
        new OperationController(accountStorage).register(server);
        Money amount = Money.of(5, "USD");
        accountStorage.deposit(1, amount);

        TransferForm transferForm = new TransferForm(1, 2, amount);
        HttpResponse<String> response = Unirest
                .post("http://localhost:1234/operation/transfer")
                .body(transferForm)
                .asString();

        assertThat(response.getStatus()).isEqualTo(HTTP_NOT_FOUND);
        assertThat(response.getBody()).isEqualTo("/accounts/2");

        assertThat(accountStorage
                .findById(1)
                .orElseThrow(NoSuchElementException::new)
                .getBalance())
                .isEqualTo(amount);
    }

    @Test
    public void shouldThrowExceptionWhenRollbackFail() {
        AccountStorage accountStorage = spy(singleAccountStorage());
        Money transferAmount = Money.of(5, "USD");
        accountStorage.deposit(1, transferAmount);
        doReturn(false).when(accountStorage).deposit(1, transferAmount); // fail rollback

        new OperationController(accountStorage).register(server);

        TransferForm transferForm = new TransferForm(1, 2, transferAmount);
        HttpResponse<String> response = Unirest
                .post("http://localhost:1234/operation/transfer")
                .body(transferForm)
                .asString();

        assertThat(response.getStatus()).isEqualTo(500);
    }

    private AccountStorage singleAccountStorage() {
        UserStorage userStorage = new UserStorage();
        userStorage.register(new UserForm("Bob", "Ross", "bob.ross@mail"));

        AccountStorage accountStorage = new AccountStorage(userStorage);
        AccountForm sourceAccount = new AccountForm(1, Monetary.getCurrency("USD"));
        accountStorage.createNewAccount(sourceAccount);

        return accountStorage;
    }

    private AccountStorage emptyAccountStorage() {
        return new AccountStorage(new UserStorage());
    }
}