package pl.kurczewski.account;

import org.javamoney.moneta.Money;
import pl.kurczewski.account.dto.Account;
import pl.kurczewski.account.dto.AccountForm;
import pl.kurczewski.account.exception.AccountNotExistsException;
import pl.kurczewski.account.exception.InsufficientBalanceException;
import pl.kurczewski.account.exception.RollbackFailedException;
import pl.kurczewski.user.UserStorage;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class AccountStorage {

    private final UserStorage userStorage;

    private ConcurrentHashMap<Long, Account> accounts = new ConcurrentHashMap<>();
    private AtomicLong idSequence = new AtomicLong(0);

    public AccountStorage(UserStorage userStorage) {
        this.userStorage = userStorage;
    }

    /**
     * Deposit money on given account
     *
     * @param accountId account id
     * @param amount    amount of money
     * @return true if account exists, false otherwise
     */
    public boolean deposit(long accountId, Money amount) {
        Account result = accounts.computeIfPresent(accountId, (id, account) -> {
            Money updatedBalance = account.getBalance().add(amount);
            return account.updateBalance(updatedBalance);
        });
        return result != null;
    }

    /**
     * Withdraw money from given account
     *
     * @param accountId account id
     * @param amount    amount of money
     * @return true if account exists, false otherwise
     * @throws InsufficientBalanceException if source account has insufficient balance
     */
    public boolean withdraw(long accountId, Money amount) throws InsufficientBalanceException {
        Account result = accounts.computeIfPresent(accountId, (id, account) -> {
            if (account.getBalance().isLessThan(amount)) {
                throw new InsufficientBalanceException();
            }
            Money updatedBalance = account.getBalance().subtract(amount);
            return account.updateBalance(updatedBalance);
        });
        return result != null;
    }

    /**
     * Transfer money between accounts, rollback if target account does not exists
     *
     * @param sourceAccountId source account id
     * @param targetAccountId target account id
     * @param amount          amount of money
     * @return true if operation succeeds, false if source account does not exists and rollback succeed
     * @throws AccountNotExistsException    if target account does not exists
     * @throws RollbackFailedException      if target account does not exists and rollback failed
     * @throws InsufficientBalanceException if source account has insufficient balance
     */
    public boolean transfer(long sourceAccountId, long targetAccountId, Money amount)
            throws AccountNotExistsException, RollbackFailedException, InsufficientBalanceException {
        if (!this.withdraw(sourceAccountId, amount)) {
            return false;
        }
        if (!this.deposit(targetAccountId, amount)) {
            if (!this.deposit(sourceAccountId, amount)) {
                throw new RollbackFailedException();
            }
            throw new AccountNotExistsException();
        }
        return true;
    }

    /**
     * @param accountForm data needed for account creation
     * @return id created for account or empty optional in case user doesn't exists
     */
    public Optional<Long> createNewAccount(AccountForm accountForm) {
        if (!userStorage.contains(accountForm.getUserId())) {
            return Optional.empty();
        }
        long id = idSequence.incrementAndGet();
        Account account = new Account(id, accountForm);
        accounts.put(id, account);
        return Optional.of(id);
    }

    public Optional<Account> findById(long id) {
        return Optional.ofNullable(accounts.get(id));
    }

    Collection<Account> getAccounts() {
        return Collections.unmodifiableCollection(accounts.values());
    }

    /**
     * @param userId user id
     * @return accounts owned by user or empty optional in case user doesn't exists
     */
    Optional<List<Account>> findByOwner(long userId) {
        if (!userStorage.contains(userId)) {
            return Optional.empty();
        }
        return Optional.of(accounts
                .values()
                .stream()
                .filter(account -> account.getUserId() == userId)
                .collect(Collectors.toList()));
    }
}